resource "digitalocean_record" "ns-www_n1" {
   domain = digitalocean_domain.default.name
   type = "NS"
   name = "@"
   value = "ns1.sohost.pl."
}

resource "digitalocean_record" "ns-www_n2" {
    domain = digitalocean_domain.default.name
    type = "NS"
    name = "@"
    value = "ns2.sohost.pl."
}