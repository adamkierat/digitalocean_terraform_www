resource "digitalocean_domain" "default" {
    name = "adamkierat.pl"
    ip_address = digitalocean_loadbalancer.www-lb.ip
}