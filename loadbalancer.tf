resource "digitalocean_loadbalancer" "www-lb" {
    name = "www-lb"
    region = "fra1"

    forwarding_rule {
        entry_protocol = "http"
        entry_port = 80

        target_protocol = "http"
        target_port = 80
    }

    healthcheck{
        protocol = "http"
        port = 80
        path = "/"
        check_interval_seconds = 60
        response_timeout_seconds = 20
        healthy_threshold = 3
        unhealthy_threshold = 3
    }
    
    droplet_ids = [
        digitalocean_droplet.www-1.id,
        digitalocean_droplet.www-2.id
    ]
    
}
