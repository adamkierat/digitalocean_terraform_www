terraform {
  backend "s3" {
    endpoint = "fra1.digitaloceanspaces.com/"
    bucket = "digitalocean-terraform-www-terraform-remote-state"
    key    = "terraform.tfstate"
    region = "us-west-1"
    skip_requesting_account_id = true
    skip_credentials_validation = true
    skip_get_ec2_platforms = true
    skip_metadata_api_check = true
  }
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {}
variable "pvt_key" {}


provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}